## Eclipse 小技巧

### 快捷键

编辑器放大缩小字体 Ctrl + +  ， Ctrl + -

自动导入包 Ctrl + 1

添加/移除 行注释 Ctrl + /

自动补全 Alt + /  比如sysout变成System.out.println main变成public static void ...

复制当前行 Ctrl + Alt + 上下方向键

移动当前行 Alt +　上下方向键

删除当前行 Ctrl + D

格式化代码 Ctrl + Shift + F

### 常见问题 FAQ

1. 乱码

修改文件属性的编码方式

Eclipse 缺省文本文件的编码为 ISO8859

网页编码一般为 UTF-8

![输入图片说明](https://images.gitee.com/uploads/images/2021/0307/181657_6cba86b7_382074.png "屏幕截图.png")