# ssm-eclipse-chapter02

#### 介绍

参考视频

B站 https://www.bilibili.com/video/BV1fb41137QF?p=1

第一章  spring 概述
第二章  spring 中的 Bean


### 运行说明

进入相关包，作为 java 程序 
 运行 TestIoC
 运行 TestDI
 运行 InstanceTest1

教材 P8,P13,P18

注意使用  java  perspective 

![输入图片说明](https://images.gitee.com/uploads/images/2021/0305/155755_1723539f_382074.png "屏幕截图.png")
