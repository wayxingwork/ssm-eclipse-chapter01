package com.itheima.annotation;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.itheima.assemble.User;
//P30 基于注解进行装配，扫描方式

public class AnnotationAssembleTest2 {

	public static void main(String[] args) {
		String xmlPath="/com/itheima/annotation/beans7.xml";
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext(xmlPath);
		UserService userService = (UserService)applicationContext.getBean("userService");
		userService.save();
	}

}
