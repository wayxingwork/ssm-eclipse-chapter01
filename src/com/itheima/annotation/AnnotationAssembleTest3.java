package com.itheima.annotation;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.itheima.assemble.User;

// 教材P32页，不使用注解进行装配。注意使用 UserController2/UserService2/UserDao2
// 需要为 UserController2 UserService2 添加 setter 方法 

public class AnnotationAssembleTest3 {

	public static void main(String[] args) {
		String xmlPath="/com/itheima/annotation/beans8.xml";
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext(xmlPath);
		UserController2 userController2 = (UserController2)applicationContext.getBean("userController2");
		userController2.save();
	}

}
