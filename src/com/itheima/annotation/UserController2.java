package com.itheima.annotation;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;

public class UserController2 {
  private UserService2 userService2;
  public UserService2 getUserService2() {
	return userService2;
}
public void setUserService2(UserService2 userService2) {
	this.userService2 = userService2;
}
public void save() {
	  this.userService2.save();
	  System.out.println("userController...save...");
  }
}
