package com.itheima.annotation;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.itheima.assemble.User;

//基于注解进行装配，和Test2类似，Controller注解

public class AnnotationAssembleTest4 {

	public static void main(String[] args) {
		String xmlPath="/com/itheima/annotation/beans9.xml";
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext(xmlPath);
		UserController userController = (UserController)applicationContext.getBean("userController");
		userController.save();
	}

}
