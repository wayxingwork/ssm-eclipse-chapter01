package com.itheima.annotation;

import javax.annotation.Resource;
import org.springframework.stereotype.Service;

public class UserService2Impl implements UserService2 {
	private UserDao2 userDao2;

	public UserDao2 getUserDao2() {
		return userDao2;
	}

	public void setUserDao2(UserDao2 userDao2) {
		this.userDao2 = userDao2;
	}

	public void save() {
		this.userDao2.save();
		System.out.println("userservice ...save...");
	}
}
