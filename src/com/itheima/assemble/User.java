package com.itheima.assemble;

import java.util.List;

public class User {
	private String username;
	private Integer password;
	private List<String> list;
	public void setUsername(String username) {
		this.username = username;
	}
	public void setPassword(Integer password) {
		this.password = password;
	}
	public void setList(List<String> list) {
		this.list = list;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "User {username="+username+",password="+password+",list="+list+"}";
	}
	
	
}
