package com.itheima.assemble;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.itheima.instance.constructor.Bean1;

public class XmlBeanAssembleTest {

	public static void main(String[] args) {
		String xmlPath="/com/itheima/assemble/beans5.xml";
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext(xmlPath);
		User user2 = (User)applicationContext.getBean("user2");
		System.out.println(user2);
		
	}

}
