package com.homework.g2021;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Demo {

	public static void main(String[] args) {
		String xmlPath="/com/homework/g2021/beans0.xml";
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext(xmlPath);
//		List<IFlyable> birdList = new ArrayList<IFlyable>();
//		birdList.add((IFlyable)applicationContext.getBean("wuya1"));
//		for(IFlyable flyable:birdList){
//			flyable.fly();
//		}
//		List<IFlyable> birdList1 = new ArrayList<IFlyable>();
//		birdList.add((IFlyable)applicationContext.getBean("fenghuang1"));
//		for(IFlyable flyable:birdList){
//			flyable.fly();
//		}
		Sky sky = (Sky)applicationContext.getBean("sky");
		sky.show();
//		System.out.println(applicationContext.getBean("wuya1"));
//		System.out.println(applicationContext.getBean("fenghuang1"));
		
	}

}
