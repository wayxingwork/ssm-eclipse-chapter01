package com.homework.g2021;

import java.util.List;

public class Sky {
	private List<IFlyable> birdList;

	public void setBirdList(List<IFlyable> birdList) {
		this.birdList = birdList;
	}
	
	public void show(){
		for (IFlyable bird : birdList){
			bird.fly();
		}
	}
}
