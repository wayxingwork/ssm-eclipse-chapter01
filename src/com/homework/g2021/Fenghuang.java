package com.homework.g2021;

public class Fenghuang implements IFlyable{
	private Integer id;

	public Fenghuang(Integer id) {
		super();
		this.id = id;
	}
	public Fenghuang() {
		super();
		
	}
	public void setId(Integer id) {
		this.id = id;
	}
	@Override
	public String toString(){
		return "凤凰：id="+ id;
	}
	public void fly() {
		System.out.println("id="+id+"凤凰飞");
	}
}
